create table continet
(
    code serial not null,
    name text   not null
);


select distinct continent
from country
order by continent asc;


insert into continet (name) --inseta valores en la tabla continet y en la columna name
select distinct continent --selecciona los valores y los inserta en la tabla continet
from country
order by continent asc;


select * from continet;


--para eliminar la restriccion de la tabla country en la columna continent
alter table country drop CONSTRAINT country_continent_check;