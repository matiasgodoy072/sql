create table public.country
(
    code           char(3) not null
        primary key,
    name           text    not null,
    continent      text    not null
        constraint country_continent_check
            check ((continent = 'Asia'::text) OR (continent = 'South America'::text) OR
                   (continent = 'North America'::text) OR (continent = 'Antarctica'::text) OR
                   (continent = 'Oceania'::text) OR (continent = 'Africa'::text) OR (continent = 'Europe'::text)),
    region         text    not null,
    surfacearea    real    not null
        constraint country_surfacearea_check
            check (surfacearea >= (0)::double precision),
    indepyear      smallint,
    population     integer not null,
    lifeexpectancy real,
    gnp            numeric(10, 2),
    gnpold         numeric(10, 2),
    localname      text    not null,
    governmentform text    not null,
    headofstate    text,
    capital        integer,
    code2          char(2) not null
);

alter table public.country
    owner to alumno;

 --para insertar datos en la tabla de respaldo
insert into country_bk
select * from country;


