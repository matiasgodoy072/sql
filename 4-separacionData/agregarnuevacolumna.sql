select *
from country_bk;


select a.name,
       a.continent,
       (select code from continet b where b.name = a.continent) --esto es una subconsulta que si coincide con el nombre de la tabla a, me devuelve el nombre de la tabla b
from country a;

update country a
set continent = (select code from continet b where b.name = a.continent);--Si la subconsulta encuentra una coincidencia, devuelve el code de continet. Este code se utiliza para actualizar el campo continent en la tabla country.
--En resumen, esta instrucción UPDATE está intentando actualizar el continent de cada país en la tabla country con el code
-- --correspondiente de la tabla continet.


select *
from country;

select *
from continet;

--instrucción está cambiando el tipo de datos de la columna continent en la tabla country a int4, y
-- convierte los datos existentes a integer para asegurarse de que son compatibles con el nuevo tipo de datos.
alter table country
alter column continent TYPE int4
USING continent::integer;


--para la columna code sean únicas asi pedo hacer una relación entre las tablas o usar code como clave primaria
ALTER TABLE continet
    ADD CONSTRAINT unique_code UNIQUE (code);

alter table country
    add constraint fk_continent_code
        foreign key (continent)
            references continet (code);

