--le estoy dificiento que el surfearea no puede recivir valores menores a 0
ALTER TABLE
    country
    ADD
        --EL SURFACEAREA NO PUEDE SER MENOR A 0 Y ES EL NOMBRE DE LA UNA COLUMNA
        check (surfacearea >= 0);



SELECT
    DISTINCT continent
from
    country;

--le digo que estos son los contitentes permidtos en la columna y puedo hacer multiples validaciones
ALTER TABLE
    country
    add
        CHECK (
            (continent = 'Asia')
                or (continent = 'South America')
                or (continent = 'North America')
                or (continent = 'Antarctica')
                or (continent = 'Oceania')
                or (continent = 'Africa')
                or (continent = 'Europe')
            );


--para eliminar la restriccion de la tabla country en la columna continent
alter table country drop CONSTRAINT country_continent_check;