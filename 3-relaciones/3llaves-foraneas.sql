--le digo que de la tabla city que la llave foranea es contycode y que la tabla que se va a relacion es county con la columna code que es la relacion
ALTER TABLE city
    add constraint fk_country_code
        foreign key (countrycode)
            references country (code);



ALTER TABLE countrylanguage
    add constraint fk_country_code
        foreign key (countrycode)
            references country (code);

--las llaves foranes son las que se relacionan con otras tablas